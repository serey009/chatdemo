package kosign.com.chatdemo.Fragments;
import kosign.com.chatdemo.Notifications.MyResponse;
import kosign.com.chatdemo.Notifications.Sender;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAPmxw-zU:APA91bGkVCxfc3Fot9QqNujlLGAeCwrMEMCIvV8c96_sVcgGxEXL0H6J-FOUuqZDgWiL7B6eLIJhfRJnHAn7V1KUBx-Em_E_y379kfn1snK1lsp9CoflxXyXFqP-v_ZJ4PkxBwSMp1uw"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
