package kosign.com.chatdemo.utils;

import android.app.ProgressDialog;
import android.content.Context;

public class CircleProgressbarDialog {

    private ProgressDialog progressBar;

    public CircleProgressbarDialog(Context context, String message) {
        progressBar = new ProgressDialog(context);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setMessage(message);
    }

    public void showProgress(){
        progressBar.show();
    }

    public void hideProgress(){
        progressBar.dismiss();
    }
}
